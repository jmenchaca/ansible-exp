# NAME: Vagrantfile
# AUTHOR: Joaquin Menchaca
# UPDATED: 2016-04-25
#
# PURPOSE: Multi-Machine Demo w/ Dynamic Config from setting in HOSTS config.
# DEPENDENCIES:
#  * VirtualBox, Vagrant
#
# -*- mode: ruby -*-
# vi: set ft=ruby :

############### CONTANTS
TIME = Time.now.strftime("%Y%m%d%H%M%S")
VAGRANT_ENV = ENV['VAGRANT_ENV'] || 'default'
CONFIGFILE_HOSTS = "./config/#{VAGRANT_ENV}.hosts"

############### BUILD DATASTRUCTURE
hosts = {}  # empty data-structure
File.readlines(CONFIGFILE_HOSTS).map(&:chomp).each do |line|
  ipaddr, hostname = line.split(/\s+/)             # only grab first two columns
  hosts[hostname] = ipaddr                         # store in hash
  PRIMARY_SYSTEM = hostname if (line =~ /primary/) # match primary
end

############### CONFIGURE VAGRANT MACHINES
Vagrant.configure("2") do |config|
  hosts.each do |hostname, ipaddr|
    default = if hostname == PRIMARY_SYSTEM then true else false end
    config.vm.define hostname, primary: default do |node|
      node.vm.box = "ubuntu/trusty64"
      node.vm.hostname = "#{hostname}"
      node.vm.network "private_network", ip: ipaddr

      node.vm.provider("virtualbox") { |vbox| vbox.name = "#{hostname}_#{TIME}" }

      # Provision Using Shell Script
      node.vm.provision "shell" do |script|
        script.env = { "VAGRANT_ENV" => VAGRANT_ENV }
        script.path = "scripts/#{hostname.split(/\./)[0]}.sh"
      end
    end
  end
end
