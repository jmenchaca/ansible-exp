**docker_support**
=========

Install `docker-py` python module on target Ubuntu system.

**Requirements**
------------

The target system is Ubuntu with Python 2.6+ and `pip` installed.


**Example Playbook**
----------------

Example:

    - hosts: servers
      roles:
         - { role: docker_support }

**License**
-------

MIT
