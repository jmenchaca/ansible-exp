**ansible**
=========

Install `ansible` tool on target Ubuntu system.

**Requirements**
------------

The target system is Ubuntu with Python 2.6+ and `pip` installed.


**Example Playbook**
----------------

Example:

    - hosts: servers
      roles:
         - { role: ansible }

**License**
-------

MIT
