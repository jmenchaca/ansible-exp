# **Ansible Experiments**

This is a homegrown environment used to bring up n number of virtual guest systems based on configuration file of your own making.

## **Provisioning Scripts**

Provisioning scripts will install Docker on Ubuntu, as well as configure `/etc/hosts` and `/etc/ssh/ssh_config` files for easy access between multiple systems.

## **Ansible Dynamic Inventory**

An Ansible dynamic inventory file called `config/inventory.py` is available, which reads the configuration hosts file can creates a dynamic inventory.  This should be used to allow Ansible to configure existing systems.

### **Testing the Inventory**

```bash
vagrant up # bring systems up
ansible all -m ping
# run a single command
ansible all -a 'lsb_release -a'
```
