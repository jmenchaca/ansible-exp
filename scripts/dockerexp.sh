#!/usr/bin/env bash
SCRIPT_PATH="/vagrant/scripts"
SCRIPTLIB="${SCRIPT_PATH}/lib"

# configure ssh_config and hosts
${SCRIPT_PATH}/setup-base.sh

. ${SCRIPTLIB}/docker.src
install_docker
